import loadVariables, { isGitLabCI } from 'gitlab-ci-variables';
 
if (isGitLabCI()) {
  console.log('Running under GitLab CI runner');
}
 
const variables = loadVariables();
// variables === undefined iff !isGitLabCI()
if (variables) {
  console.log(`Build (ID ${variables.build.id}) of ${variables.project.name}. Started by ${variables.user.email}.`);
}